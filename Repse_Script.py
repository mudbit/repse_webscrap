from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
#from selenium.webdriver.chrome.options import Options
from datetime import datetime

def ConsolePrint(strMessage):
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print(dt_string + ": " + strMessage)


# Send a GET request to the website
url = 'https://repse.stps.gob.mx/'

""" # Configure Chrome options for headless mode
chrome_options = Options()
chrome_options.add_argument('--headless')  # Run Chrome in headless mode
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')

# Initialize the WebDriver with the configured Chrome options
driver = webdriver.Chrome(options=chrome_options) """

driver = webdriver.Chrome()

# Wait for 2 seconds
time.sleep(2)

# Open the web page
driver.get(url)

ConsolePrint("Open site")
# Wait until the page is ready (e.g., wait for the presence of an element)
time.sleep(5)
wait = WebDriverWait(driver, 10)
element = wait.until(EC.presence_of_element_located((By.XPATH, "//div[@id='arrowdiv-consulta']")))

ConsolePrint("Get items")
print(element.get_attribute("innerHTML"))

# Find the clickable div element using Selenium
driver.implicitly_wait(10)

time.sleep(5)
ConsolePrint("Then click div")
element.click()
time.sleep(5)

driver.implicitly_wait(10)

print("Get Elements")
# Locate elements where the CSS class contains 'btn-main' using XPath
btn_Search = driver.find_elements(By.XPATH, "//button[contains(@class, 'btn-continue') and @type='submit']")

# Iterate through the elements and perform actions
for element in btn_Search:
    print(element.get_attribute("innerHTML"))  # Example action: print the text of each element
    if "Consultar" in element.get_attribute("innerHTML"):
        btn_Consulta = element


time.sleep(5)
ConsolePrint("Then click consultar")
btn_Consulta.click()

time.sleep(5)
ConsolePrint("Write it down")
input_field = wait.until(EC.presence_of_element_located((By.XPATH, "//input[@id='rsoc']")))
input_field.send_keys("HUMAN FACTOR DEL NORTE")

time.sleep(5)
ConsolePrint("Then click buscar")
btn_Buscar = driver.find_element(By.XPATH, "//button[@id='bnt_busqueda']")
btn_Buscar.click()

time.sleep(20)
ConsolePrint("Then Search for")

tbl_Result = wait.until(EC.presence_of_element_located((By.XPATH, "//table[@id='tablaem']")))
tbl_Content = tbl_Result.find_elements(By.TAG_NAME, "tr")

print(tbl_Result.get_attribute("innerHTML"))

for row in tbl_Content:
    cells = row.find_elements(By.TAG_NAME, "td")
    for cell in cells:
        print(cell.get_attribute("innerHTML"))


# Close the WebDriver
driver.quit()
